﻿/*
Source Host           : localhost:3306
Source Database       : proj

*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acc_types`
-- ----------------------------
DROP TABLE IF EXISTS `acc_types`;
CREATE TABLE `acc_types` (
`id_type`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(25) CHARACTER SET cp1250 COLLATE cp1250_general_ci NOT NULL DEFAULT 'nazwa' ,
PRIMARY KEY (`id_type`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
COMMENT='typy kont'
AUTO_INCREMENT=30

;

-- ----------------------------
-- Records of acc_types
-- ----------------------------
BEGIN;
INSERT INTO `acc_types` VALUES ('1', 'wplywy'), ('2', 'wydatki'), ('3', 'portfel'), ('4', 'kredytowe');
COMMIT;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
`id_owner`  int(11) NOT NULL DEFAULT 1 ,
`id_acc`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(25) CHARACTER SET cp1250 COLLATE cp1250_general_ci NOT NULL DEFAULT 'nazwa/opis' ,
`suma`  decimal(10,2) NOT NULL DEFAULT 0.00 ,
`id_curr`  int(11) NOT NULL DEFAULT 1 ,
`id_type`  int(11) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id_acc`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
COMMENT='tabela zawiera wpisy na temat kont (wpĂ„Ä…Ă˘â‚¬Ĺˇywy, wydatki, por'
AUTO_INCREMENT=28

;

-- ----------------------------
-- Records of accounts
-- ----------------------------
BEGIN;
INSERT INTO `accounts` VALUES ('1', '1', 'przychody', '15450.00', '1', '1'), ('1', '2', 'wydatki', '-3200.00', '1', '2'), ('1', '3', 'portfel', '0.00', '1', '3'), ('1', '4', 'skarbonka', '700.00', '1', '4'), ('1', '17', 'kredyty', '4541.00', '1', '27'), ('12', '20', 'przychody', '20923.00', '23', '1'), ('12', '21', 'kasa na lewo', '0.00', '24', '3'), ('12', '25', 'samochod', '3914.30', '23', '4'), ('12', '26', 'Ojro', '30000.00', '24', '1'), ('12', '24', 'wydatki', '2791.00', '23', '2');
COMMIT;

-- ----------------------------
-- Table structure for `currency`
-- ----------------------------
DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
`id_curr`  int(11) NOT NULL AUTO_INCREMENT ,
`curr_name`  varchar(25) CHARACTER SET cp1250 COLLATE cp1250_general_ci NOT NULL DEFAULT 'nazwa waluty' ,
`sign`  char(3) CHARACTER SET cp1250 COLLATE cp1250_general_ci NULL DEFAULT NULL ,
`exchange`  decimal(10,4) UNSIGNED ZEROFILL NOT NULL DEFAULT 000000.0000 ,
`last_update`  date NOT NULL DEFAULT '1999-01-01' ,
`id_owner`  int(11) NOT NULL ,
PRIMARY KEY (`id_curr`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
COMMENT='obsĂ„Ä…Ă˘â‚¬Ĺˇugiwane waluty i kurs Ă„Ä…Ă˘â‚¬Ĺźredniej wymiany NBP; aktu'
AUTO_INCREMENT=27

;

-- ----------------------------
-- Records of currency
-- ----------------------------
BEGIN;
INSERT INTO `currency` VALUES ('1', 'zloty', 'PLN', '000001.0000', '2013-12-23', '1'), ('2', 'euro', 'EUR', '000004.4401', '2013-12-25', '1'), ('3', 'dolar amerykanski', 'USD', '000003.3980', '2013-12-23', '1'), ('4', 'frank szwajcarski', 'CHF', '000003.6301', '2013-12-24', '1'), ('5', 'funt szterling', 'GBP', '000005.3232', '2013-12-23', '1'), ('22', 'peso meksykanske', 'PHP', '000000.0781', '2013-12-25', '1'), ('23', 'Zloty', 'PLN', '000001.0000', '2014-01-12', '12'), ('24', 'Euro', 'EUR', '000004.4645', '2014-01-11', '12'), ('26', 'Dolar amerykanski', 'USD', '000004.1234', '2014-01-12', '12');
COMMIT;

-- ----------------------------
-- Table structure for `debts`
-- ----------------------------
DROP TABLE IF EXISTS `debts`;
CREATE TABLE `debts` (
`id_debt`  int(11) NOT NULL AUTO_INCREMENT ,
`description`  varchar(100) CHARACTER SET cp1250 COLLATE cp1250_general_ci NULL DEFAULT 'opis' ,
`debt`  decimal(10,2) NOT NULL DEFAULT 0.00 ,
`debt_cost`  decimal(10,2) NOT NULL ,
`left2repay`  decimal(10,2) NOT NULL DEFAULT 0.00 ,
`id_curr`  int(11) NOT NULL DEFAULT 1 ,
`id_owner`  int(11) NOT NULL DEFAULT 0 ,
`data`  date NOT NULL ,
`id_acc`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id_debt`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
COMMENT='w tej tabeli bÄ‚â€žĂ˘â€žËdÄ‚â€žĂ˘â‚¬Â¦ siÄ‚â€žĂ˘â€žË znajdowaÄ‚â€žĂ˘â‚¬Ë‡ wyĂ„Ä…Ă˘â‚¬ĹˇÄ‚â€žĂ˘â‚¬Â¦'
AUTO_INCREMENT=32

;

-- ----------------------------
-- Records of debts
-- ----------------------------
BEGIN;
INSERT INTO `debts` VALUES ('28', 'kredyt na samochod', '15000.00', '2171.86', '17171.86', '1', '1', '2013-11-18', '17'), ('31', 'kredyt na samochod', '20000.00', '3914.30', '23914.30', '23', '12', '2014-01-12', null);
COMMIT;

CREATE OR REPLACE VIEW V_debts AS SELECT debts.id_owner, debts.description, debts.debt, debts.left2repay, currency.sign, debts.data, debts.debt * currency.exchange AS przeliczone FROM `debts` INNER JOIN `currency` ON debts.id_curr = currency.id_curr;
COMMIT;

-- ----------------------------
-- Table structure for `expanses`
-- ----------------------------
DROP TABLE IF EXISTS `expanses`;
CREATE TABLE `expanses` (
`id_item`  int(11) NOT NULL AUTO_INCREMENT ,
`id_acc`  int(11) NOT NULL DEFAULT 0 ,
`cost`  decimal(10,2) NOT NULL DEFAULT 0.00 ,
`data`  date NOT NULL DEFAULT '1999-01-01' ,
`id_curr`  int(11) NOT NULL DEFAULT 1 ,
`description`  char(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`id_owner`  int(11) NOT NULL ,
PRIMARY KEY (`id_item`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
COMMENT='zawiera historiÄ‚â€žĂ˘â€žË wydatkĂ„â€šÄąâ€šw'
AUTO_INCREMENT=20

;

-- ----------------------------
-- Records of expanses
-- ----------------------------
BEGIN;
INSERT INTO `expanses` VALUES ('14', '2', '2200.00', '2014-01-07', '1', 'HTC Sensation', '1'), ('12', '2', '1000.00', '2013-12-12', '2', 'prezenty pod choinke', '1'), ('11', '2', '1000.00', '2013-12-26', '1', 'telewizor', '1'), ('18', '24', '250.00', '2014-01-12', '23', 'zegarek', '12'), ('17', '24', '4000.00', '2014-01-11', '23', 'laptop', '12'), ('19', '24', '1000.00', '2013-12-20', '23', 'prezenty', '12');
COMMIT;

CREATE OR REPLACE VIEW V_exp AS SELECT expanses.id_owner, expanses.description, expanses.cost, currency.sign, expanses.data, expanses.cost * currency.exchange AS przeliczone FROM `expanses` INNER JOIN `currency` ON expanses.id_curr = currency.id_curr;
COMMIT;

-- ----------------------------
-- Table structure for `incomes`
-- ----------------------------
DROP TABLE IF EXISTS `incomes`;
CREATE TABLE `incomes` (
`data`  date NOT NULL DEFAULT '0000-00-00' ,
`id_acc`  int(11) NOT NULL ,
`id_inc`  int(11) NOT NULL AUTO_INCREMENT ,
`description`  varchar(25) CHARACTER SET cp1250 COLLATE cp1250_general_ci NOT NULL DEFAULT 'opis' ,
`amount`  decimal(10,2) NOT NULL DEFAULT 0.00 ,
`id_curr`  int(11) NOT NULL DEFAULT 1 ,
`exchange`  decimal(10,4) NULL DEFAULT 1.0000 ,
`id_owner`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id_inc`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
COMMENT='info o przychodach'
AUTO_INCREMENT=60

;

-- ----------------------------
-- Records of incomes
-- ----------------------------
BEGIN;
INSERT INTO `incomes` VALUES ('2014-01-03', '1', '40', 'gwiazdka', '450.00', '1', '1.0000', '1'), ('2013-12-10', '1', '38', 'wyplata za 11/11', '10000.00', '1', '1.0000', '1'), ('2014-01-11', '20', '56', 'pensja za 12/11', '8000.00', '23', '1.0000', '12'), ('2014-01-11', '20', '55', 'prezent od babci', '150.00', '23', '1.0000', '12'), ('2014-01-11', '20', '57', 'premia', '300.00', '23', '1.0000', '12'), ('2014-01-12', '26', '58', 'sprzedane prawa autorskie', '30000.00', '24', '1.0000', '12');
COMMIT;


CREATE OR REPLACE VIEW V_inc AS SELECT incomes.id_owner, incomes.description, incomes.amount, currency.sign, incomes.data, incomes.amount * currency.exchange AS przeliczone FROM `incomes` INNER JOIN `currency` ON incomes.id_curr = currency.id_curr; 
COMMIT;

-- ----------------------------
-- Table structure for `owners`
-- ----------------------------
DROP TABLE IF EXISTS `owners`;
CREATE TABLE `owners` (
`id_owner`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(25) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL DEFAULT 'imie' ,
`2nd_name`  varchar(30) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL DEFAULT 'nazwisko' ,
`login`  varchar(20) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL DEFAULT 'login' ,
`haslo`  varchar(50) CHARACTER SET cp1250 COLLATE cp1250_polish_ci NOT NULL DEFAULT '' ,
`mail`  varchar(30) CHARACTER SET cp1250 COLLATE cp1250_general_ci NOT NULL DEFAULT 'e@mail.com' ,
PRIMARY KEY (`id_owner`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=cp1250 COLLATE=cp1250_general_ci
COMMENT='tabela zawiera imiÄ‚â€žĂ˘â€žË, nazwisko oraz identyfikator wĂ„Ä…Ă˘â‚¬Ĺˇa'
AUTO_INCREMENT=13

;

-- ----------------------------
-- Records of owners
-- ----------------------------
BEGIN;
INSERT INTO `owners` VALUES ('1', 'Jan', 'Kowalski', 'jan', 'a8393058e7f0735a5578a6f288c388dc', 'jkowal@poczta.ru'), ('2', 'Michal', 'Zielony', 'michal', 'c06cc1e3e996b8696ecc3d1f307806d1', 'mrgreeny@cubespace.jp'), ('12', 'Rafal', 'Pytkowski', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'rafalpytkowski@gmail.com');
COMMIT;

-- ----------------------------
-- Auto increment value for `acc_types`
-- ----------------------------
ALTER TABLE `acc_types` AUTO_INCREMENT=30;

-- ----------------------------
-- Indexes structure for table accounts
-- ----------------------------
CREATE INDEX `id_owner` ON `accounts`(`id_owner`) USING BTREE ;
CREATE INDEX `id_curr` ON `accounts`(`id_curr`) USING BTREE ;
CREATE INDEX `id_type` ON `accounts`(`id_type`) USING BTREE ;

-- ----------------------------
-- Auto increment value for `accounts`
-- ----------------------------
ALTER TABLE `accounts` AUTO_INCREMENT=28;

-- ----------------------------
-- Auto increment value for `currency`
-- ----------------------------
ALTER TABLE `currency` AUTO_INCREMENT=27;

-- ----------------------------
-- Indexes structure for table debts
-- ----------------------------
CREATE INDEX `id_curr` ON `debts`(`id_curr`) USING BTREE ;
CREATE INDEX `id_owner` ON `debts`(`id_owner`) USING BTREE ;

-- ----------------------------
-- Auto increment value for `debts`
-- ----------------------------
ALTER TABLE `debts` AUTO_INCREMENT=32;

-- ----------------------------
-- Indexes structure for table expanses
-- ----------------------------
CREATE INDEX `id_acc` ON `expanses`(`id_acc`) USING BTREE ;
CREATE INDEX `id_curr` ON `expanses`(`id_curr`) USING BTREE ;

-- ----------------------------
-- Auto increment value for `expanses`
-- ----------------------------
ALTER TABLE `expanses` AUTO_INCREMENT=20;

-- ----------------------------
-- Indexes structure for table incomes
-- ----------------------------
CREATE INDEX `id_acc` ON `incomes`(`id_acc`) USING BTREE ;
CREATE INDEX `id_curr` ON `incomes`(`id_curr`) USING BTREE ;

-- ----------------------------
-- Auto increment value for `incomes`
-- ----------------------------
ALTER TABLE `incomes` AUTO_INCREMENT=60;

-- ----------------------------
-- Auto increment value for `owners`
-- ----------------------------
ALTER TABLE `owners` AUTO_INCREMENT=13;
