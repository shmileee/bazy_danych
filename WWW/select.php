<?php
// -- Funkcja generująca listy --
	function generateSelect($name = '', $options = array()) 
	{
		$html = '<select name="'.$name.'">';
		foreach ($options as $option => $value)
      {
        $html .= '<option value='.$value.'>'.$option.'</option>';
      }
		$html .= '</select>';
		return $html;
	}
// -- END --
?>